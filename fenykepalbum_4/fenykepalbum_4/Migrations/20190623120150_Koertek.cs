﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace fenykepalbum_3.Migrations
{
    public partial class Koertek : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Kepek",
                columns: new[] { "Id", "Filenev", "Holkeszult", "Leiras", "Mikor" },
                values: new object[] { 1, "WP_20150531_15_50_32_Pro.jpg", "Lednice", "Kastély park, tó", new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Kepek",
                columns: new[] { "Id", "Filenev", "Holkeszult", "Leiras", "Mikor" },
                values: new object[] { 2, "WP_20150531_16_01_58_Pro.jpg", "Lednice", "Kastély park, tó", new DateTime(2015, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Kepek",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Kepek",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
